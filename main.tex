%!TeX spellcheck = en-GB

% Font size
\documentclass[12pt,a4paper,british]{article}

\usepackage{ifxetex}

\ifxetex
  \usepackage{fontspec}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{babel}
  \usepackage{lmodern}
\fi

\usepackage[margin=1in]{geometry}

\usepackage{hyperref}

\usepackage{csquotes}

% Referencing
\usepackage[backend=biber,date=year,alldates=year,urldate=short,bibstyle=apa,maxcitenames=2,uniquelist=false,citestyle=authoryear,block=ragged]{biblatex}
\DeclareLanguageMapping{english}{english-apa}
\AtEveryCite{%
  \clearfield{month}%
  \clearfield{day}%
  \clearfield{labelmonth}%
  \clearfield{labelday}%
  \clearfield{labelendmonth}%
  \clearfield{labelendday}%
}

\bibliography{references.bib}

% break bilbatex reference URL on number
\setcounter{biburlnumpenalty}{9000}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr} 

% Epigraph
\usepackage{epigraph}

% length of epigraph
\setlength\epigraphwidth{12cm}
% remove epigraph bar
\setlength\epigraphrule{0pt}

\renewcommand{\epigraphsize}{\large}

\usepackage{etoolbox}

\makeatletter
\patchcmd{\epigraph}{\@epitext{#1}}{\itshape\@epitext{#1}}{}{}
\makeatother


% license
\usepackage[
    type={CC}, 
    modifier={by-nc-sa},
    version={4.0},
]{doclicense}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{
  Russian Neopatrimonialism \\
  \vspace{0.5cm}
  \large What are the origins of neopatrimonial practices in Russia, and how do they affect contemporary politics and society?
}

\author{\textit{2270405d}}

 % Date, use \date{} for no date
\date{\today}


% command to count words in document
\newcommand\wordcount{
  \immediate\write18{
    texcount -inc -sum -0 -template={SUM} main.tex | xargs echo -n > count.txt
    }
  \input{count.txt}words
}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontal header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{Russian Politics - Essay}
% right hand side header
\rhead{\docauthor}
\setlength{\headheight}{15pt}

% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

%------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%------------------------------------------------------------------------

% Uncomment to change the name of the abstract to something else
\renewcommand{\abstractname}{Essay -\wordcount}

\begin{abstract}
  % abstract
\end{abstract}

\vspace*{\fill}

% \epigraph{``the tumultuous course of economic shock therapy in Russia is a well-documented chapter of contemporary history. It is, how­ever, a story too often told in the bland language of `reform', a narrative so generic that it has hidden one of the greatest crimes committed against a democracy in modern history.''}{\fullcite[220]{kleinShockDoctrineRise2007}}

\epigraph{``Corruption wasn't an intruder to Russia's free-market reforms''}{\small\fullcite[241]{kleinShockDoctrineRise2007}}

\vspace*{\fill}


\pagebreak

%------------------------------------------------------------------------
%	BODY
%------------------------------------------------------------------------

\section*{} %introduction

In Moscow on October 23\textsuperscript{rd} 1993, Warren Christopher then U.S. Secretary of State declared : ``The United States does not easily support the suspension of parliaments. But these are extraordinary times'' \autocite[777]{christopherNewGenerationRussian1993}.
He made this extraordinary remark while on mission to Russia to support Boris Yelstin, president of the Russian Federation, days after the latter had ordered the shelling of the Russian parliament.
As this essay will demonstrate, the U.S. holds a very special place in the creation of the Russian oligarchy and the evolution of its neopatrimonial state.

We provide an overview of neopatrimonial practices in the Russian leadership, tracing its origins to the creation of superpresidentialism and the implementation of shock therapy reforms in post-soviet Russia.
We compare how neopatrimonialism evolved from the oligarchic influences on the Yeltsin government to the ``business capture'' by the power vertical that emerged from the Putin presidencies.
We analyse how this evolving neopatrimonialism gas shaped national economic giants who have in turn influenced political decisions, weakening the voice of opposing oligarchs with a reified bureaucracy as well as civil society through parallel institutions.
Lastly, we also discuss the rhetoric of Russian ``Sovereign Democracy'' crafted as an answer to Russian Neopatrimonialism and built upon a strong state to ``defend'' Russia from foreign intervention thus shaping an alternative conception of the State differing from normalized conceptions of democracy.

\section*{The rise of the oligarchic state}

\subsection*{Centralization of power and new Russian presidentialism}

The constitution of the first Republic of the Russian Federation weaved in the idea of separation of powers that were emerging during Gorbachev's constitutional reforms of 1988.
This notion of power sharing had already begun to erode under the diarchy of the Communist Party and the Soviet State  \autocite{sharletRussianConstitutionalCrisis1993}.
Perhaps the most illustrative example of this dual state erasing notions of seperations of power was the attempted 1991 August Putsch by Communist Party members desperate to abort the increasing dominance of Gorbachev's Executive over the Party.

At the end of that same year, the Soviet Union was dissolved, forming the First Russian Republic which lasted only two years.
The inherited power sharing between the Russian legislature and executive coupled with the push for autonomy from the regions of the Federation were at the heart of tensions between the institutions.
Tensions which culminated in the 1993 siege of the Russian Legislature marking the end of the First Russian Republic \autocite{sharletRussianConstitutionalCrisis1993}.

\paragraph{}

What followed was a recentralization of power in the hands of the executive, shifting constitutional inspirations from the semi-presidentialism of American federalism to the centralized presidentialism emanating from the constitution of the French fifth republic \autocite{kleinPresidentsOligarchsBureaucrats2016,breslauerRussianDemocraticRevolution2012}.
From the French constitution, Russia inherited key presidential powers like the rule by decree as well as the ability to dissolve parliament.
There was no parliamentary majority in the first elected Duma of this new Russian Republic but given the prominence of the presidency, it did not matter \autocite{chauvierOctobre1993Liberalisme2014,kleinPresidentsOligarchsBureaucrats2016}.

The Yeltsin presidency surrounded itself with a ministerial cabinet of technocrats, ``Russia's Chicago Boys'' as the press called them \autocite[222-223]{kleinShockDoctrineRise2007}.
Among them were notable neoliberal Russian economists like Andrey Illarionov (who will later become Putin's economic advisor) or Yegor Gaïdar as vice prime minister later succeeded by Anatoli Tchoubaïs (later under Putin: head of the Russian national Energy Distribution company) \autocite{chauvierOctobre1993Liberalisme2014}.

\subsection*{From ``Shock Therapy'' to Capital consolidation}

The United States government quickly came to reinforce these liberal ideological foundations, notably with a \$2.1 million contract to the Harvard Institute for International Development to advise the Russian government on the economic transition \autocite{kleinShockDoctrineRise2007,mcclintickHowHarvardLost2005}.
At its head: Andrei Shleifer, a Harvard economist ultimately involved in a U.S. Government lawsuit against the University settled for a total of \$26.5 million.
Shleifer was accused of having personally benefited from his reshaping of the Russian economy through his wife's investements in the Federation based on early information on the upcoming reforms.

From Harvard's ``advice'' came the idea of a ``populist privatization'' \autocite[100]{boyckoPrivatizingRussia1997} by issuing vouchers to Russian citizens to be traded for cash or used to obtain shares in the soon-to-be privatized 225,000 state owned enterprises \autocite{bivensRussiaYouNever1998,besserieTempsOligarquesEp2018,mcclintickHowHarvardLost2005,kleinShockDoctrineRise2007,boyckoPrivatizingRussia1993}.

Perhaps underlining the inadequacy of liberal voucher trading in a post-communist state, each one of them had a list of ``options'' explaining what to do with them, notably noting the possibility of centralizing these vouchers in ``voucher funds'' for ``efficient ownership'' \autocite[94-95]{boyckoPrivatizingRussia1997}.
Moreover, their face value of 10,000 roubles convertible to cash was an option favoured by 72\% of Russian citizens \autocite[8]{kogutCapitalMarketDevelopment2002}.

Harvard's scholars' own estimates suggests that 45 of the 144 Million vouchers (a third) were acquired by 600 voucher funds \autocite[100]{boyckoPrivatizingRussia1997} with among them funds introduced by company managers to try to regain control of ``their'' privatized shares.
The funds became the legal owners of the acquired capital with original voucher holders relegated to owning a share of the fund itself \autocite{appelVoucherPrivatisationRussia1997}.

The opening of market forces as suggested by the Harvard advisers Federation happened without regulation of investment funds.
According to these Harvard economists ``The principal accomplishment of privatization has been the massive retreat of the government from economic activity, and the transfer of control rights from politicians to managers and investors.'' \autocite[121-122]{boyckoPrivatizingRussia1997}.

\paragraph*{}

This ``retreat of government'' allowed for some of Russia's economic crown jewels to be privatized discreetly to a few individuals.
It favoured those with insider knowledge by having ``authorized banks'', allowed to hold pulic funds, use this money in the auctions they were tasked with organizing and acquire the undervalued public assets up for sale \autocites[622,623]{bivensRussiaYouNever1998}[233]{kleinShockDoctrineRise2007}[328]{grahamOligarchyOligarchyStructure1999}.
The strategic companies' shares were also held as collateral by these select private banks, in case the Russian government defaulted on its debt, in what became known as the ``loans for shares'' program devised by Vladimir Potanin \autocites[627]{bivensRussiaYouNever1998}[4]{goldmanPiratizationRussiaRussian2003}.

Norilsk Nickel was privatized as such, it is a mining company and was a key driver of the Russian economy which ``sold for \$170 million — even though its profits alone soon reached \$1.5 billion annually. \autocite[232]{kleinShockDoctrineRise2007} to \textit{Uneximbank}.
\textit{Uneximbank} was a private banking firm ``hastily incorporated'' by Potanin, also First Deputy Prime Minister of Russia, which ``was tasked'' with accepting bids for the auction of Norilsk Nickel.
Simultaneously, Potanin through his holding firm \textit{Interros} \autocite{TooMuchTrouble1998}, was offering the considerably undervalued albeit winning \$170 million bid while a competing offer, double the size, ``was judged unfit'' \autocite[628]{bivensRussiaYouNever1998}.

Uneximbank, also indirectly became majority shareholder in the Sidanko oil company which ``went for \$130 million; just two years later that stake would be valued on the international market at \$2.8 billion.'' \autocites[232]{kleinShockDoctrineRise2007}[628]{bivensRussiaYouNever1998} thus purchasing 4,820 million barrel of oil equivalent (boe) worth of reserves for an astounding 0.03\$/boe (the international rate for reserves was 4\$/boe) \autocites[223]{khartukovPotentialRussianState2000}[628]{bivensRussiaYouNever1998}.
That same month, Stolichny Savings Bank and the Financial Oil Corporation (FNK), the latter a front company of Boris Berezovsky, acquired a 51\% stake in the Sibneft oil company, purchasing 2,045 million boe at 0.05\$/boe \autocite[223]{khartukovPotentialRussianState2000}.
Berezovsky, Deputy Secretary of Russia’s Security Council under Yeltsin, would later claim that a group of seven bankers including him, the \textit{``Semibankirschina''} controlled the majority of the Russian economy \autocites[2]{goldmanPiratizationRussiaRussian2003}{freelandMoscowGroupSeven1996}.

\paragraph*{}

This practice of secretive privatizations favouring key individuals with insider knowledge coupled with abundant under-evaluation of privatized government assets led to concentration of ex-public capital into the hands of a few individuals \autocite{besserieTempsOligarquesEp2018,sapirTransitionRusseVingt2012}.
\textcite[231]{kleinShockDoctrineRise2007} aptly summarizes post-soviet privatizations in Russia:

\begin{quote}
  \textit{``A clique of nouveaux billionaires, many of whom were to become part of the group universally known as ``the oligarchs'' for their imperial levels of wealth and power, teamed up with Yeltsin's Chicago Boys and stripped the country of nearly everything of value''}
\end{quote}

\subsection*{Purchasing the Russian Presidency}

This proximity between the business elite and the presidency allowed Yeltsin to be re-elected despite the unfavourable polls: the economic and media support of the so-called ``oligarchs'' backed his declining public figure in exchange for public property through the ``loans-for-share'' programs \autocite{breslauerRussianDemocraticRevolution2012,besserieTempsOligarquesEp2018,kleinShockDoctrineRise2007}.
This new economic elite took advantage of the concentrated political authority, weakening to unfavourable public opinion, to exert its influence on the decision making process, essentially purchasing power with wealth turning Russia into a ``plutocracy'' \autocite{raviotRussianPostSovietOligarchy2015}.

The Government of Moscow at the turn of the millennium was an example, with American diplomatic cables underlining that ``the extent corruption in Moscow remains pervasive with Mayor Luzhkov at the top of the pyramid'' \autocite[Par.~14]{beyrleLuzkhovDilemma2010}.
The extent of this `pyramid' also protected him as ``he could link others in the government to the corruption''  \autocite{beyrleLuzkhovDilemma2010}.

\textcite[329]{grahamOligarchyOligarchyStructure1999} identifies other oligarch coalitions within the political circles at the dusk of the Yeltsin presidencies: the Chernomyrdin coalition built around Energy industry conglomerates, Kozhakov and Soskovets strong through their hold on the mining and arms export sector and lastly the Chubays and Berezovsky duo who operated the principal medias in Russia.
The latter helped Yeltsin win the presidency over the communist candidate Gennady Zyuganov favoured by the polls in 1996 ``thanks to an esti­mated \$100 million in financing from oligarchs (thirty-three times the legal amount) as well as eight hundred times more coverage on oligarch-controlled TV stations than his rivals.'' \autocites[232]{kleinShockDoctrineRise2007}[1]{goldmanPiratizationRussiaRussian2003}.

By the end of the Yeltsin era, the Russian Federation had gone through a consolidation of political power in the hands of the executive, particularly the presidency.
This was followed by the rise of an economic elite at the expense of wealth redistribution in a post-communist state which weakened the ideological legitimacy of the state.
The ``maximization of power in politics and the maximization of rents in the economy should be perceived as a rational goal of the ruling groups, who achieved it in Russia in the wake of regime changes and market reforms.'' \autocite[459]{gelmanViciousCirclePostSoviet2016}.
To avoid being removed by the electorate, the political elite traded the Russian public assets for the economic and media support of the ``oligarchy''.
This was the keystone of the bridge between political and economic circles, thus stacking the last brick of Russian Neopatrimonialism.

\subsection*{Conceptualizing ``Neopatrimonialism''}

Behind the mouthful term ``neopatrimonialism'' lies a form of political domination, in the Weberian sense of legitimacy: the foundation for state authority  \autocite[Appendix A]{weberEconomySocietyNew2019}.
``Neopatrimonialism is a mixture of two, partly interwoven, types of domination that co-exist: namely, patrimonial and legal-rational bureaucratic domination'' \autocite[18]{erdmannNeopatrimonialismRevisitedCatchAll2006}.

Neopatrimonialism is a modern instance of Weberian patrimonialism \autocite[369]{weberEconomySocietyNew2019} where, unlike the latter, there exists a distinction, albeit not a separation, between public ``legal-rational'' domination and private ``patrimonial'' domination.
Moreover, a political system meeting these characteristics only truly becomes neopatrimonial when the actors of both of these spheres intertwine but a formal distinction remains `on paper' \autocite{erdmannNeopatrimonialismProblemsCatchall2012}.

In this sense, the institutions of the second Russian Republic which centralized power in the presidency were a favourable ground for the creation of a network of influence around the president.
The result is an embodiment of neopatrimonialism: ``patrimonialism—personal claims to power, ties and relations—are combined with, and complemented, complicated and conflicted by, impersonal institutions, which have some existence independent of individual political actors'' \autocite[349]{robinsonRussianNeopatrimonialismPutin2017}.
This was particularly true in under Yelstin: the presidency, as an institution, was more powerful than the president overshadowed by the influential oligarchs.
Particularly so at the end of his presidency, when Yeltsin's ability to act as an arbiter between factions was overturned and centroids of influence competing around him progressively took over the presidency by propping up its failing president \autocite[355]{robinsonRussianNeopatrimonialismPutin2017}.
\textcite[35]{dufyAporiesTransitologieQuelques2013} note that neopatrimonialism can accommodate any regime so long as institutions are weak and power personalisation and its networks are strong.
As we have explained, this was the case of the Russian Federation, whose incarnation of power lied in a powerful presidency surrounded with institutions weakened by the influence of an economic elite.

\section*{The Putinian State}

Despite this centralization of influence, some have noted the failure of the economic policies implemented by the Yeltsin presidency to construct a robust Russian financial sector \autocite[198]{sapirTransitionRusseVingt2012}.
In summer 1998 a couple of weeks before the economic crisis, Goldman Sachs sold \textit{en masse} its Russian short term government bonds (``GKO''), a trigger which led many owners of the same bonds to doubt the solvency of the Russian debt and follow suite \autocite[200]{sapirTransitionRusseVingt2012}.
This came in a context of decreasing oil prices, the literal fuel for the Russian economy, and the South East Asia crisis of 1997 which spilled over into the Federation's economy \autocite[99-102]{sapirTransitionRusseVingt2012}.
Ultimately, the Government lost control of its currency and devalued the rouble which had been pegged to the dollar, wiping out the savings of many Russians with an 84\% inflation rate and doubling food prices \autocite{raviotRussianPostSovietOligarchy2015}.

It was in this context that Yeltsin resigned, passing the baton to his Prime Minister, Vladimir Putin.
Putin's presidency is contrasted with Yeltsin's 90s, through its advocacy for a ``dictatorship of the law'' \autocite{gelmanDictatorshipLawRussia2012}, particularly with regard to the Russian oligarchy and its extralegal practices.
This transition was permitted by an Economic Recovery thanks to the rouble devaluation and favourable oil prices which resulted in multiple percentage points of yearly GDP growth in early 2000s: this tilted balance of power in favour of Putin \autocite{besserieTempsOligarquesEp2018,yakovlevEvolutionBusinessState2006}.
Initially, he established a policy of ``equidistance'' \autocite[Chap.~3]{sakwaPutinOligarchKhodorkovskyYukos2014} between the oligarchy and the state: he would not reverse the privatizations of the 1990s while they would not interfere in politics \autocite[11]{raviotRussianPostSovietOligarchy2015}.
Where the Yeltsin years were an era of ``state capture'' by the new big businesses, the Putin period soon shifted from its principle of ``equidistance'' to one of ``business capture'' by the State \autocites[1048]{yakovlevEvolutionBusinessState2006}[93]{volkovReseauxPoliticoeconomiquesDans2017}.

\subsection*{Economic patriotism, the bedrock of a ``Sovereign Democracy''}

Halfway through the first decade of the 21\textsuperscript{th} Century, the Russian Federation's tax code and trade legislation had grown complex to the point where ``even the most law abiding [enterprise], is bound to break some rules and can be penalised on a provable ground'' \autocite[1048]{yakovlevEvolutionBusinessState2006}.

Transgressing his promise of a ``dictatorship of the Law'', Putin constructed a ``dictatorship of the will of the state'' through the threat of legal conviction in a complex legal business framework thus inverting the balance of power in his favour.
Indeed, the ``bureaucratic consolidation'' of the Russian state enabled it to force the now subordinate business sector to abide by its decisions or face conviction if the state desired \autocite[1053]{yakovlevEvolutionBusinessState2006}.

The very mediatised signal that Putin had consolidated enough influence to take on the oligarchy was perhaps the arrest of Mikhail Khodorkovsky, chairman of the Yukos fossil energy company on October 25\textsuperscript{th} 2003 \autocite{sakwaPutinOligarchKhodorkovskyYukos2014}.
It is important to remember the post-Yeltsin context whereby ``the widespread development of informal relations and survival strategies bypassing formal social and constitutional institutional structures generated a type of alternative state, [\ldots] since the official state was incapable of doing its job properly, Yukos executives claimed the moral right to create their own'' \autocite[48]{sakwaPutinOligarchKhodorkovskyYukos2014}.
In May 2013, Yukos signed a merger agreement to absorb Sibneft, which would create the fourth largest oil company in the world \autocite{sakwaPutinOligarchKhodorkovskyYukos2014}.
In the midst of this deal, Khodorkovsky met with American vice-president Dick Cheney to discuss the sale of a block of shares 25 to 40\% of the future oil giant.
This would buy Khodorkovsky American protection and support in Russia, much needed especially because his political meddling ``to ensure a strong ‘Yukos’ lobby in parliament'' \autocite[59]{sakwaPutinOligarchKhodorkovskyYukos2014} was in violation of the principle of equidistance.
Putin countered this danger of foreign influence on Russian oil with his coalition of \textit{siloviki}, notably the deputy director of the Federal Security Service (FSB) who built a case against Yukos for tax evasion which was later picked up by the General Prosecutor.
The latter ultimately brought up seven charges against the Yukos chairman, among them tax evasion and embezzlement of billions of roubles, halting the merger of the two oil giants and the potential sale to American companies of 39\% of Russia's oil production \autocite[59]{sakwaPutinOligarchKhodorkovskyYukos2014}.

\paragraph*{}

This was a turning point in the neopatrimonial state-business relations: under Yeltsin, the ``oligarchs'' were invited in to government to pilot economic reforms as they saw fit.
Under Putin, the state became the orchestrator of patrimonial capital, striking a balance between hindering rebellious oligarchs while maintaining strong vertically integrated corporations as economic assets to the Russian state.
This form of ``economic patriotism'' \autocite[93]{volkovReseauxPoliticoeconomiquesDans2017} is, in the state's discourse, a tool of governance: essentially subordinating corporations and the oligarchs of a time passed to the interest of the Russian State.
State control of flagship economic giants has led the official discourse of the Russian state to coin the now infamous notion of Russia as a ``Sovereign Democracy'' explained in \textcite[see: 28/06/06 brief]{surkovSurkovHisOwn2006}:

\begin{quote}
  \textit{``We want to be a free nation among other free nations and to cooperate with them proceeding from fair principles, we don't want to be managed from abroad.''}
\end{quote}

Where some see ``neopatrimonialism'', others underline, not only the strength of the Russian presidency, but also Putin's influence in the economy as an asset in the defence of the national interest.
In the face of acts of economic warfare like the robbery of Alstom, the French energy and transport giant shadily acquired by General Electric aided by the American department of Justice \autocite{pierucciPiegeAmericain2019}, one can only appreciate the discourse of a ``Sovereign Democracy''.

\paragraph*{}

Contrarily to France's, the Russian government recognizes this, for example in the Arctic, where Global Warming has rendered the exploitation of hydrocarbons north of the Federation feasible \autocite{descampsGeopolitiqueBriseglace2020}.
Some of these projects require resources and expertise not readily available to Russian companies, notably in the case of the Yamal LNG coenterprise exploiting the natural gas reserves in Sabetta.
To solve this, France's Total, Petrochina and the Silk Road sovereign fund own close to half of the project and bring in needed expertise and investment but the Russian state retains a form of oversight through the controlling 50.1\% share of the Novatek gas company \autocite{descampsGeopolitiqueBriseglace2020,ottYamalLNGMeeting2019a}.

Novatek is not the main actor in Russia's LNG market (20\% of Russian gas production) but is the main counterbalancing weight for Putin against the Gazprom giant, especially since one of his director and investor, Gennady Timchenko has been noted to have close ties to Putin \autocites{jackstubbsManWhoMarried2015}[207]{orttungPromotingSustainabilityRussia2015}

\subsection*{``A centralised state that only has the interests of the elites at heart''}

Indeed, for Putin, ``the last step in consolidating the vertical of power was the abolition of the direct elections of the governors.'' \autocite[69]{kleinPresidentsOligarchsBureaucrats2016}.
As a result ``after 2001, tax and budget legislation, not the subsoil law, defined the distribution of resource rents between the federal and regional governments and since these were federally controlled, the distribution began to heavily favour the federal government'' \autocite[209]{oxenstiernaChallengesRussiaPoliticized2015}.
Local governments have thus become increasingly reliant on private investors which ``have little interest in sustainability''. Migration patterns in the YaNAO show that individuals come and go based on the price of gas, establishing a strong link between the volatile health of the gas industry and the health of the region \autocite{oxenstiernaChallengesRussiaPoliticized2015}.

Thus, ``Putin did everything possible to establish a rigid ‘power vertical’, to completely do away with all checks and balances'' \autocite[69]{kleinPresidentsOligarchsBureaucrats2016}, not merely by controlling the funds of local governments but also restricting their political power through a decree creating the federal districts of the Russian Federation, enabling the Federal government to appoint regional governors \autocite[1043]{yakovlevEvolutionBusinessState2006}.

\subsection*{Sovereign Democracy: Business as usual?}

Environmental activist Yevgeniya Chirikova has called Russia ``A centralised state that only has the interests of the elites at heart'' \autocite{chirikovaRussiaItGetting2016}. This is a position she has personally experienced, as Putin's judo partner Arkadiy Rotenberg lobbied the prime minister for support on the Khimki highway project to which she was opposed \autocite[15]{petrovThreeDilemmasHybrid2014}. Ultimately, police forces removed protestors from the Khimki forest and protected the deforestation and the construction of the highway.

Then president Dmitry Medvedev attempted some mediation on the issue through debates in the ``Civic Chamber of the Russian Federation'', an institution bringing together public figures and NGOs for democratic deliberation \autocite[16]{petrovThreeDilemmasHybrid2014}.
This institution serves the purpose of circulating ``feedback between citizens and the state'' \autocites[466]{gelmanViciousCirclePostSoviet2016}[16]{petrovThreeDilemmasHybrid2014} but is in reality ``designed both to attract and to contain initiatives from below in ways that would strengthen the state'' \autocites[42]{richterPutinPublicChamber2009}[1567]{gilbertCrowdingOutCivil2016}.

It is easy to spot the cracks in the democractic facade of initiatives like the Civic Chamber or the notion of Sovereign Democracy, which some have noted is more sovereign than democratic as in practice ``the state emerges as an entity separate and autonomous from society'' \autocite[44]{richterPutinPublicChamber2009}.
This comes back to the Weberian notion of legal-rational domination whereby instead of \textit{being} the state, Russians are under its authority.
In fact this authority is, under Putin, much more personalized than it ever was under Yeltsin whose authority emanated from the institutional powers of the presidency: under President Putin, the authority of the presidency radiates from Putin's public personality.
In this regard and especially compared to the 1990s, the Putin presidencies lean more towards patrimonialism than neoaptrimonialism because influence has shifted from institutions to individuals, one in particular \autocite[356]{robinsonRussianNeopatrimonialismPutin2017}.

This notion of a ``network state'' \autocite[2-3]{kononenkoRussiaNetworkState2011} gravitating around Putin has been explored by \textcite{volkovReseauxPoliticoeconomiquesDans2017} who models this social network composed of multi-millionaires and \textit{siloviki} thus forming the \textit{korporatura}.
Among them, 18 invididuals are associated with 136 companies, 15 of them making up 20\% of Russian GDP and sharing a strong ``administrative capital'', the source of their political influence.
Despite moving towards patrimonialism through the personalization of the presidency, the Russian state remains a neopatrimonial state because of these networks of influence around the Russian institutions.
Moreover, neopatrimonialism lies precisely at this intersection between the formal and informal state, and this junction is blurry in Russia as the many private-public ``interlocks''  \autocite[92]{volkovReseauxPoliticoeconomiquesDans2017} create ``uncertainty about what is private and what is public in
Russia and where to locate the state along this continuum'' \autocite[164]{kononenkoRussiaNetworkState2011}.

\paragraph*{}

In the early years of the Putin presidency, favourable economic conditions cemented his legitimacy and have given him political maneuvering room \autocites[442-443]{robinsonInstitutionalFactorsRussian2012}[5]{petrovThreeDilemmasHybrid2014}.
Today however the economic winds are not as favourable to Putin upon whose personality rests the stability of politco-economic system \autocites[21]{petrovThreeDilemmasHybrid2014}[107]{volkovReseauxPoliticoeconomiquesDans2017}.
Moreover, it is wrong to assume a Russian preference for autocracy, \textcite{haleMythMassRussian2011} concludes, in line with the theory of Soveign Democracy, that Russians favour ``a kind of democracy that nevertheless relies on electing a strong leader as a way of concentrating national efforts on the resolution of major national challenges'' \autocite[1372]{haleMythMassRussian2011}.

\section*{Neopatrimonialism: yesterday, today\dots  Tommorow?}

As we have seen, today's neopatrimonial politics in Russia stem from landmark moments in Russia's recent history.

The first was the tensions between parliament and the presidency in the First Russian Republic which resulted in the shelling of the former and rise of superpresidentialism in Russia.

The second was the botched post-Soviet economic reforms assisted by American economists whose voucher privatization, price deregulation and loans-for-share programs resulted in incredible concentrations of capital in an emerging ``oligarchy''.

As the political and economic spheres intertwined, the oligarchs took over, battling each other for influence over policymaking and ultimately hijaking the decision process from the presidency creating a neopatrimonial intertwining of politico-economic circles.

\paragraph*{}

The turn of the millenium was marked by a deep economic recession in Russia, the ensuing devaluation of the rouble as well as favourable economic markets fuelled the rise of Putin to the presidency.
The neopatrimonial balance of power evolved under Putin who, unlike his predecessor, made full use of the constitution's superpresidentialism by personifiying the presidency.

Consolidating the bureaucracy around a network of \textit{siloviki} and businessmen, he exiled those who did not accept the shift of power from the oligarchy to the presidency.
Another consequence of the establishment of a power vertical by Putin is that civil society has seen a recession in its ability to voice issues by being relegated to parallel and deliberately inconsequential institutions.
Meanwhile, those among Russia's richest who accepted this shift of neopatrimonialism closer to patrimonialism have benefited from an economic rent as well as preferential albeit supervised access to the pesidency.

Russia became characterised as a ``Sovereign Democracy'', a term used in official state discourse to underline that it is a free nation free from outside influence.
This independance requires a strong state to protect Russian Interest, a conception which challenges unilateral ideas of democracies for some while being a thinly veiled justification for neopatrimonial rule for others.

\subsection*{} %conclusion

%------------------------------------------------------------------------
%   BIBLIOGRAPHY   
%------------------------------------------------------------------------

\newpage

\section*{References}

\printbibliography[heading=none]

\vspace*{\fill}

% print license
\doclicenseThis

\end{document}
